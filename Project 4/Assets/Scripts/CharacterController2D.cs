using UnityEngine;
using UnityEngine.Events;

public class CharacterController2D : MonoBehaviour
{
	public float jumpForce = 400f;				// Jump speed.
    public float movementSmoothing = .05f;	    // How much to smooth out the movement
	private bool airControl = false;							// Whether or not a player can steer while jumping;
	public LayerMask whatIsGround;							// what is ground to the character
	public Transform groundCheck;							// A position marking where to check if the player is grounded.
				

	const float groundedRadius = .2f;                     
	private bool grounded;                                // Whether or not the player is grounded.
    private Rigidbody2D rg;
	private bool facingRight = true;                      // For determining which way the player is currently facing.
	private Vector3 velocity = Vector3.zero;

	[Header("Events")]
	[Space]

	public UnityEvent OnLandEvent;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	private void Awake()
	{
		rg = GetComponent<Rigidbody2D>();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();
	}

	private void FixedUpdate()
	{
		bool wasGrounded = grounded;
		grounded = false;

		
		Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadius, whatIsGround);
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
			{
				grounded = true;
				if (!wasGrounded)
					OnLandEvent.Invoke();
			}
		}
	}
    private void Flip()
    {
        // Switch the way facing.
        facingRight = !facingRight;


        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void Move(float move, bool jump)
	{
        // jump...
        if (grounded && jump)
        {
            // Add a vertical force to the player.
            grounded = false;
            rg.AddForce(new Vector2(0f, jumpForce));
        }

        //only control the player if grounded or airControl is turned on
        if (grounded || airControl)
		{

			// Move 
			Vector3 targetVelocity = new Vector2(move * 10f, rg.velocity.y);
			
			rg.velocity = Vector3.SmoothDamp(rg.velocity, targetVelocity, ref velocity, movementSmoothing);

			// Flip
			if (move > 0 && !facingRight)
			{
				
				Flip();
			}
			
			else if (move < 0 && facingRight)
			{
				
				Flip();
			}
		}
		
	}

}
