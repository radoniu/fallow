﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

    public Dialog diolog;

    public void TriggerDialog()
    {
        FindObjectOfType<DialogManager>().StartDialog(diolog);
    }
}
