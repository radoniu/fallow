﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private GameManager gm;
    public CharacterController2D controller;
    public Animator animator;

    public float runSpeed = 40f;

    float move = 0f;
    bool jump = false;
    


    void Start()
    {
        GameObject gameControllerObject =
            GameObject.FindWithTag("GameController");

        gm = gameControllerObject.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

        move = Input.GetAxisRaw("Horizontal") * runSpeed;

        animator.SetFloat("Speed", Mathf.Abs(move));

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;    
            animator.SetBool("IsJumping", true);
        }
        
    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
    }

    void FixedUpdate()
    {
        // Move our character
        controller.Move(move * Time.fixedDeltaTime, jump);
        jump = false;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            gm.DecrementLives();
                    
        }
        if (collision.gameObject.tag.Equals("Goal"))
        {
            gm.Win();

        }
    }

}



