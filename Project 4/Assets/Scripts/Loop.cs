﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loop : MonoBehaviour {

    public float loopSpeed;
    public Transform imageposition;
    public float reset;
    Vector3 origin;
    


    // Use this for initialization
    void Start () {
        origin = imageposition.position;
       
	}
	
	// Update is called once per frame
	void Update () {
          
        transform.Translate((new Vector3(-1, 0, 0)) * loopSpeed);

        if (transform.position.x < reset)
        {
            transform.position = origin;
        }
        
    }
}
