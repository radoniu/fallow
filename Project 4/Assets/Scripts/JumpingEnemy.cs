﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingEnemy : MonoBehaviour {
    
    public float jumpForce = 400f;
    private GameManager gameController;
    public CharacterController2D controller;
    public float jumpDelay = 4f;
 

    // Use this for initialization
    void Awake()
    {
        GameObject gameControllerObject =
         GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
        Invoke("Jump", jumpDelay);
        
    }

    void Jump()
    {
        controller.Move(0, true);
    }
    void OnCollisionEnter2D(Collision2D colision)
    {
        if (colision.gameObject.tag.Equals("Player"))
        {


            gameController.DecrementLives();
        }

    }

}
