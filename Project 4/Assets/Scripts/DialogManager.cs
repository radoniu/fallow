﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour {
    public Text title;
    public Text dialogue;
    private Queue<string> sentence;

    public Animator animator;

    void Start()
    {
        sentence = new Queue<string>(); 
    }
    public void StartDialog(Dialog dialog)
    {
        animator.SetBool("isOpen", true);
        title.text = dialog.name;

        sentence.Clear();

        foreach(string words in dialog.sentences)
        {
            sentence.Enqueue(words);
        }

        DisplayNext();
    
    }
    public void DisplayNext()
    {
        if(sentence.Count == 0)
        {
            EndDialog();
            return;
        }

        string words = sentence.Dequeue();
        dialogue.text = words;

    }
    public void EndDialog()
    {
        animator.SetBool("isOpen", false);
    }
}
